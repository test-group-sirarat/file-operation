       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. SIRARAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION.
       FD  EMP-FILE.
       01  EMP-DETAILS.
           88 END-OF-EMP-FILE VALUE HIGH-VALUE .
           05 EMP-SSN PIC 9(9).
           05 EMP-NAME .
               10 EMP-SURNAME PIC X(15).
               10 EMP-FORENAME PIC X(10).
           05 EMP-DATE-OF-BIRTH .
              10 EMP-YOB  PIC 9(4).
              10 EMP-MOB  PIC 9(2).
              10 EMP-DOB  PIC 9(2).
           05 EMP-GENDER PIC  X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE 
           MOVE "123456789" TO EMP-SSN 
           MOVE "YAIMADEUA" TO EMP-SURNAME 
           MOVE "SIRARAT" TO  EMP-FORENAME .
           MOVE "20000721" TO EMP-DATE-OF-BIRTH 
           MOVE "F" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           MOVE "987654321" TO EMP-SSN 
           MOVE "KIM" TO EMP-SURNAME 
           MOVE "DOYOUNG" TO  EMP-FORENAME .
           MOVE "19960201" TO EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           MOVE "543219876" TO EMP-SSN 
           MOVE "LEE" TO EMP-SURNAME 
           MOVE "JENO" TO  EMP-FORENAME .
           MOVE "20000201" TO EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS 
           CLOSE EMP-FILE 
           GOBACK 
           .